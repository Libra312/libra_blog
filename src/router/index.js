import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/pages/login/index.vue'
import Register from '@/pages/register/index.vue'

import Home from '@/pages/home/index.vue'
import Book from '@/pages/book/index.vue'
import Lab from '@/pages/lab/index.vue'
import Contact from '@/pages/contact/index.vue'
import Blog from '@/pages/blog/index.vue'
Vue.use(Router)
export default new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [{
    path: '/',
    component: Home
  }, {
    path: '/login',
    name: 'login',
    component: Login
  }, {
    path: '/register',
    name: 'register',
    component: Register
  }, {
    path: '/book',
    name: 'book',
    component: Book
  }, {
    path: '/home',
    name: 'home',
    redirect: '/'
  }, {
    path: '/lab',
    name: 'lab',
    component: Lab
  }, {
    path: '/blog',
    name: 'blog',
    component: Blog
  }, {
    path: '/contact',
    name: 'contact',
    component: Contact
  }]
})