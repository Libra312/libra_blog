import axios from 'axios'
import qs from "qs"
// import router from '@/router/index.js'

// baseUrl
// let baseDeUrl = `localhost:3009`
// let basePrUrl = `www.libra.com`

const instance = axios.create({
  baseURL: 'http://localhost:3009',
  timeout: 10000,
  headers: {
    "Content-Type": "application/x-www-form-urlencoded;charset=utf-8"
  }
})

// 请求拦截器
instance.interceptors.request.use(
  config => {
    if (config.method.toLocaleLowerCase() === "post") {
      // 序列化
      config.data = qs.stringify(config.data)
    }
    // 每次发送请求之前判断是否存在token，如果存在，则统一在http请求的header都加上token，不用每次请求都手动添加
    // 即使本地存在token，也有可能token是过期的，所以在响应拦截器中要对返回状态进行判断
    // const token = store.state.token     
    // token && (config.headers.Authorization = token)
    return config
  },
  error => {
    return Promise.error(error)
  }
)
// // 响应拦截器
// instance.interceptors.request.use(
//   response => {        
//     if (response.status === 200) {            
//         return Promise.resolve(response)
//     } else {            
//         return Promise.reject(response)   
//     }    
//   },
//   error => {
//     if (error.response.status) {            
//       checkStatus(error.response.status)
//       return Promise.reject(error.response)     
//     } 
//   }
// )
// /** 
//  * 跳转登录页
//  * 携带当前页面路由，以期在登录页面完成登录后返回当前页面
//  */
// const toLogin = () => {
//   router.replace({
//       path: '/login',        
//       query: {
//           redirect: router.currentRoute.fullPath
//       }
//   })
// }
// // 根据不同响应状态码 做出相应响应
// const checkStatus = status => {
//   switch (status) {                
//     // 401: 未登录                
//     case 401:                    
//       toLogin()
//       break
//     // 403 token过期 或则 无权限              
//     // 登录过期对用户进行提示                
//     // 清除本地token和清空vuex中token对象                
//     // 跳转至无权限页面，5s后自动跳转至登录页面                
//     case 403:                     
//       // 403 逻辑                   
//       break
//     // 404请求不存在                
//     case 404:                    
//       // 404 逻辑
//       break             
//     // 其他错误，直接抛出错误提示                
//     default:                    
//       // 其他错误           
//   }          
// }
export default instance