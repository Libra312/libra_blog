// 随机生成 在 left 到 right 区间内的 n 个随机整数
const generatorRandom = (n, left, right) => {
  if (left >= right) {
    [left, right] = [right, left]
  }
  left = Math.floor(left)
  right = Math.floor(right)
  let arr = Array.of(n)
  for (let i = 0; i < n; i++) {
    arr[i] = Math.floor(Math.random() * (right - left + 1) + left)
  }
  return arr
}
// 倒计时函数
const countDown = (second) => {
  second -= 1
  if (second===0) {
    return
  }
  setTimeout (() => {
    countDown(second)
  }, 1000)
}
export {
  generatorRandom,
  countDown
}
