const doc = document
// 拆分域名 www.libra.com => [www,libra,com]
const domain = window.location.hostname.split('.')
// 获取 baseDomain => libra.com
const baseDomain = domain[1] + '.' + domain[2]

const cookie = {
  // 写cookies
  // document.cookie = "name=value; expires=expiredays;path=/;domain=www.libra.com;"
  setCookie: function setCookie (name, value, expiredays, domain) {
    var exdate = new Date(expiredays)
    var expires = isNaN(exdate) ? '' : ';expires=' + exdate.toGMTString()
    doc.cookie = name + '=' + value + expires + ';path=/;domain=.' + (domain || baseDomain)
  },

  // 读取cookies
  getCookie: function getCookie (name) {
    // (^| )匹配开头和空格  ([^;]*)匹配 非;  (;|$)')匹配;和结尾
    var reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)')
    var arr = doc.cookie.match(reg)
    if (arr) {
      return arr[2]
    } else {
      return ''
    }
  },

  // 删除cookies
  delCookie: function delCookie (name, domain) {
    // 过期时间设置为当前时间之前的时间来使cookie因过期而被清空
    var exp = new Date()
    var cval = ' '

    exp.setTime(exp.getTime() - 100)
    doc.cookie = name + '=' + cval + ';expires=' + exp.toGMTString() + ';path=/;domain=.' + (domain || baseDomain)
  },
  // 清空cookies
  clearCookie: function clearCookie (domain) {
    var keys = doc.cookie.split(';')
    if (keys) {
      for (let i = keys.length; i--;) {
        var exp = new Date()
        exp.setTime(exp.getTime() - 100)
        doc.cookie = keys[i].split('=')[0] + '=" ";expires=' + exp.toGMTString() + ';path=/;domain=.' + (domain || baseDomain)
      }
    }
  }
}

export default cookie
