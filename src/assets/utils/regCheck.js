const RegNumber = /^[0-9.]*$/
// 验证手机号
const RegPhone = /^(13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9]|19[0-9])\d{8}$/
// 验证邮箱
const RegEmail = /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/
// 验证空格
const RegSpace = /(\s+)/g
// 验证url
const RegUrl = /^(http[s]?:\/\/)([\S]*)$/
// 验证身份证号
const RegIdCardNumber = /(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}$)/

export {
  RegNumber,
  RegSpace,
  RegEmail,
  RegPhone,
  RegUrl,
  RegIdCardNumber
}
