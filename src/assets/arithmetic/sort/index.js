// 选择排序
const selectionSort = (arr) => {
  let len = arr.length
  for (let i = 0; i < len; i++) {
    let minIndex = i
    for (let j = i + 1; j < len; j++) {
      if (arr[j] < arr[minIndex]) {
        minIndex = j
      }
    }  
    [arr[i], arr[minIndex]] = [arr[minIndex], arr[i]]
  }
  return arr
}
// 插入排序
const insertSort = (arr) => {
  let len = arr.length
  for(let i = 1; i < len; i++) {
    for(j = i; j > 0 && arr[j] < arr[j-1]; j--) {
      [arr[j], arr[j-1]] = [arr[j-1], arr[j]]
    }
  }
  return arr
}
const generatorRandom = (n, left, right) => {
  if (left >= right) {
    [left, right] = [right, left]
  }
  left = Math.floor(left)
  right = Math.floor(right)
  let arr = Array.of(n)
  for (let i = 0; i < n; i++) {
    arr[i] = Math.floor(Math.random() * (right - left + 1) + left)
  }
  return arr
}
// 冒泡排序
const bubbleSort = (arr) => {
  let len = arr.length
  for(let i = 0; i < len -1; i++) {
    for (let j = 0; j < len - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        [arr[j], arr[j+1]] = [arr[j+1], arr[j]]
      }
    }
  }
  return arr
}
// 希尔排序
const shellSort = (arr) => {
  let len =arr.length
  gap = Math.floor(len/2)
  while(gap!==0){
    for(let i = gap;i<len;i++){
        let temp = arr[i]
        let j;
        for(j=i-gap;j>=0&&temp<arr[j];j-=gap){
            arr[j+gap] = arr[j]
        }
        arr[j+gap] = temp
    }
    gap=Math.floor(gap/2)
  }
  return arr;
}
console.time('运行时间')
bubbleSort(generatorRandom(100000,1000,10000))
console.timeEnd('运行时间')