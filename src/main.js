import Vue from 'vue'
import App from './App.vue'
import vMessage from '@/components/notification/index.js' 
import instance from '@/assets/utils/request.js'
import router from '@/router'
// 引入
import MarkdownRun from 'vue-markdown-run'
// 全局注入
Vue.use(MarkdownRun)
Vue.use(vMessage)
Vue.config.productionTip = false
Vue.prototype.$axios = instance

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
