# 盒子模型

> 图片、表单元素一律看作是文本，它们并不是盒子

盒子模型包括标准盒子模型和`IE`盒子模型

## 标准盒子模型

属性`width`,`height`只包含内容`content`，不包含`border`和`padding`。

![image](https://libra321.oss-cn-huhehaote.aliyuncs.com/blog/box2.png)

## `IE`盒子模型

属性`width`,`height`包含`border`和`padding`，指的是`content`+`padding`+`border`。

![image](https://libra321.oss-cn-huhehaote.aliyuncs.com/blog/box.png)

##  盒模型的转换

- `box-sizing` 属性用于更改用于计算元素宽度和高度的默认的 `CSS` 盒子模型

- `content-box`: 默认值，标准盒子模型。 `width`与 `height` 只包括内容的宽和高
- `border-box`: `width`和 `height` 属性包括内容，内边距和边框，但不包括外边距

## 关于边距

- 内边距`padding`、边框`border`和外边距`margin`默认值是零。但是，浏览器样式表（浏览器缺省值）设置外边距和内边距。因此可以将元素的 `margin` 和 `padding` 设置为零来覆盖这些浏览器样式。

- 外边距可以是负值，而且在很多情况下都要使用负值的外边距，内边距不可以是负值。

> `margin` 属性的值设为负值即负边距，在 `CSS` 布局中是一个很有用的技巧。当 `margin-top `、 `margin-left` 为负值的时候，会把元素上移、左移，同时文档流中的位置也发生相应变化，这点与 `position:relative` 的元素设置 `top`、`left` 后元素还占据原来位置不同。当 `margin-bottom` 、 `margin-right` 设为负值的时候，元素本身没有位置变化，后面的元素会下移、右移

![image](https://libra321.oss-cn-huhehaote.aliyuncs.com/blog/margin%E4%B8%8Erelative.gif)

  - 使用`margin`的负值利用绝对定位居

    ```html
    <div class="book">
      <div class="container">
        <div class="item"></div>
      </div>
    </div>
    ```
    ```css
    .container {
      ...
      width: 200px;
      height: 200px;
      position: relative;
    }
    .item {
      ...
      width: 100px;
      height: 100px;
      position: absolute;
      top: 50%;
      left: 50%;
      margin-top: -50px;
      margin-left: -50px;
    }
    ```
    ![image](https://libra321.oss-cn-huhehaote.aliyuncs.com/blog/margin-center.png)
      - 负边距对于浮动元素的影响

```html
<div id="box">
  <div class="float"></div>
  <div class="float"></div>
  <div class="float"></div>
</div>
```


不加负边距 

```css
#box .float:nth-child(3) {
  ...
}
```

  ![image](https://libra321.oss-cn-huhehaote.aliyuncs.com/blog/float-nomargin.png)

加入负边距（第三个元素覆盖了第二个元素 `20px` ，经典的多列布局正是利用此原理）

```css
#box .float:nth-child(3) {
  background: blue;
  margin-left: -22px;
}
```

  ![image](https://libra321.oss-cn-huhehaote.aliyuncs.com/blog/float-margin.png)